#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

//������� ������
struct StringItem {
	const char* str; //��������� �� ������
	StringItem* next;
};

//pList - ������ ������ ���������� �� ������
//iDigit - ������, �� �������� ���������
//���������� ��������� �� ������ ������� ��������������� ������������������
StringItem* radix_sort_msd_for_string(StringItem* pList, unsigned int iDigit)
{
	//���������� ��������� �������� ������ ������� (char)
	const int iRange = 256;

	//������ ����������
	StringItem* front[iRange];
	memset(front, 0, sizeof(front));

	StringItem** ppNextItem[iRange];
	for (int i = 0; i < iRange; i++)
		ppNextItem[i] = &front[i];

	//��������� ������ �� bucket'�, � ����������� �� �������
	while (pList)
	{
		StringItem* temp = pList;
		pList = pList->next;

		temp->next = NULL; //��������� �� ������

		unsigned char c = (unsigned char)temp->str[iDigit];
		*ppNextItem[c] = temp;
		ppNextItem[c] = &temp->next;
	}

	//������ �������� ������
	StringItem* pResult = NULL;
	StringItem** ppNext = &pResult;

	//������� bucket ���������� ���� - �� ��� ������������
	*ppNext = front[0];
	while (*ppNext)
		ppNext = &((*ppNext)->next);

	for (int i = 1; i < iRange; i++)
	{
		//������ - ����������
		if (!front[i])
			continue;

		if (front[i]->next == NULL) //� ����� ��������� - ����� ���������
			*ppNext = front[i];
		else
			*ppNext = radix_sort_msd_for_string(front[i], iDigit + 1);

		while (*ppNext)
			ppNext = &((*ppNext)->next);
	}

	return pResult;
}

void TestSort()
{
	//��������� ������� ��������� ���� ��� �����
	ifstream fin("demo.txt");
	if (!fin)
		return;

	//��������� ��� ����� �� ����� � ������
	istream_iterator<string> ii(fin);
	istream_iterator<string> eos;
	vector<string> vecs(ii, eos);

	// ��������� ������
	StringItem* pList = NULL;
	StringItem** ppCurr = &pList;
	for (unsigned int i = 0; i < vecs.size(); i++)
	{
		*ppCurr = new StringItem();
		(*ppCurr)->str = vecs[i].c_str();
		(*ppCurr)->next = NULL;
		ppCurr = &(*ppCurr)->next;
	}

	//���������
	pList = radix_sort_msd_for_string(pList, 0);

	//���� ��� ������
	ofstream fout("out.txt");
	ostream_iterator<string> oo(fout, " ");

	// � ����� ������� ������
	while (pList)
	{
		oo = pList->str; // ������� � ����
		StringItem* tmp = pList;
		pList = pList->next;
		delete tmp;
	}
}

int main()
{
	TestSort();

	return 0;
}