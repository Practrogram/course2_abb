//������� ����������� ��������

#include <iostream>
#include <fstream>

int main()
{
	std::ifstream in, fin1, fin2;//������ ������� ��� ��������� �����
	std::ofstream out, fout1, fout2;//������ ������� ��� ��������� ������

	int a = 0, b = 0; //���������� ��� ����� � ������
	int size = 0; //���-�� ����� � �����
	int count = 0, Acount = 0, Bcount = 0;
	bool flag = true;
	bool AFull = 0, BFull = 0;

	in.open("input.txt");
	out.open("output.txt");

	while (in >> a)
	{
		out << a << " ";
		size++;
	}

	in.close();
	out.close();
	/*����� ����� ����������� �� ������ ����, ������ � ���������
	� ��������� � ����������. � �������� ����� ��� ����� ����� 
	����� 1, ����� ������� ���� ����� ��������� 2, ����� ������� - 4,
	����� �������� - 8, ����� k-�� - 2k*/
	for (int partSize = 1; partSize < size; partSize *= 2)
	{
		//�������� ���� ��������� �� ��� ��������������� �����
		in.open("output.txt");
		fout1.open("A.txt");
		fout2.open("B.txt");
		count = 0;
		while (in >> a)
		{
			count++;
			if (flag)
				fout1 << a << " ";
			else
				fout2 << a << " ";

			if (count == partSize)
			{
				count = 0;
				flag = !flag;
			}
		}
		in.close();
		fout1.close();
		fout2.close();

		fin1.open("A.txt");
		fin2.open("B.txt");
		out.open("output.txt");

		//��������������� ����� ������� � ���� ����
		//��� ���� ��������� �������� �������� ������������� ����
		if (fin1 >> a)
			AFull = true;
		else
			AFull = false;
		if (fin2 >> b)
			BFull = true;
		else
			BFull = false;
		//���������� ���� ����� ��������������
		//��� ���� ������������� ���� ��������� � ������������� �������
		for (int i = 0; i < size; i += 2 * partSize)
		{
			Acount = 0, Bcount = 0;
			while (Acount < partSize && AFull && Bcount < partSize && BFull)
				if (a < b)
				{
					out << a << " ";
					if (fin1 >> a)
						AFull = true;
					else
						AFull = false;
					Acount++;
				}
				else 
				{
					out << b << " ";
					if (fin2 >> b)
						BFull = true;
					else
						BFull = false;
					Bcount++;
				}
			//�������� ����, ������� ������� � �������� � �. �.
			//������ ��� �������� ����� ������ ������������������� �� ��� ���, ���� �� ����� ���������� ������� ���� ����
			while (Acount < partSize && AFull)
			{
				out << a << " ";
				if (fin1 >> a)
					AFull = true;
				else
					AFull = false;
				Acount++;
			}
			while (Bcount < partSize && BFull)
			{
				out << b << " ";
				if (fin2 >> b)
					BFull = true;
				else
					BFull = false;
				Bcount++;
			}
		}

		fin1.close();
		fin2.close();
		out.close();
		remove("A.txt");
		remove("B.txt");
	}

	return 0;
}