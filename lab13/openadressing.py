class HashTable:
    def init(map, ts):
        map.size = ts
        map.slots = [None] * map.size
        map.data = [None] * map.size
    def put(map, key, data):
        hash = map.hashfunction(key, len(map.slots))

        if map.slots[hash] == None:
            map.slots[hash] = key
            map.data[hash] = data
        else:
            if map.slots[hash] == key:
                map.data[hash] = data  #replace
            else:
                nextslot = map.rehash(hash, len(map.slots))
                while map.slots[nextslot] != None and \
                      map.slots[nextslot] != key:
                    nextslot = map.rehash(nextslot, len(map.slots))

                if map.slots[nextslot] == None:
                    map.slots[nextslot] = key
                    map.data[nextslot] = data
                else:
                    map.data[nextslot] = data #replace

    def hashfunction(map, key, size):
        return key%size

    def rehash(map, oldhash, size):
        return (oldhash+1)%size
    def get(map, key):
        startslot = map.hashfunction(key, len(map.slots))

        data = None
        stop = False
        found = False
        position = startslot
        while map.slots[position] != None and  \
        not found and not stop:
            if map.slots[position] == key:
                found = True
                data = map.data[position]
            else:
                position = map.rehash(position,len(map.slots))
                if position == startslot:
                    stop = True
        return data


#main
ma = []

with open('input.txt','r') as file:
    for line in file:
        for word in line.split():
            ma.append(word)

Ht = HashTable(len(ma))

for key in range(len(ma)):
    Ht.put(key, ma[key])


f = open('hashtable.txt', 'w')
f.write("========== HashTable ==========\n")
for i in range (len(Ht.slots)):
    f.write(str(Ht.slots[i]))
    f.write(": ")
    f.write(Ht.data[i])
    f.write("\n")
f.close()

