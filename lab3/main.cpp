#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Rus");
	int x;
	cout << "Введите число x: ";
	cin >> x;
	cout << endl;

	double a = log(x) / log(3);
	double b = log(x) / log(5);
	double c = log(x) / log(7);
	int K = (int)trunc(a);
	int L = (int)trunc(b);
	int M = (int)trunc(c);

	for (int k = 0; k <= K; k++)
		for (int l = 0; l <= L; l++)
			for (int m = 0; m <= M; m++) {
				int i = pow(3, k) * pow(5, l) * pow(7, m);
				if (i <= x)
					cout << i << " ";
			}

	return 0;
}