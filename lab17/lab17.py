import os

class Node:
    def init(elem, data=None, left=None, right=None):
        elem.data = data
        elem.left = left
        elem.right = right


    def __str__(elem):
        return str(elem.data)

class BinarySearchTree:
    def init(elem, expression):
        elem.root = None
        elem.add(expression)

    def add(elem, expression):
        nums = '0123456789'
        num = ''
        token_list = []
        for n in expression:
            if n in nums:
                num += n
            elif num:
                token_list.append(int(num))
                num = ''
                token_list.append(n)
            else:
                token_list.append(n)

        elem.root = elem._addToTree(token_list)

    def _addToTree(elem, token_list):
        if token_list:
            brackets = 0
            index_sym = 0
            for inx, sym in enumerate(token_list):
                if sym == '(':
                    brackets += 1
                    if brackets == 1:
                        for i in range(len(token_list)):
                            if token_list[i] == ',' and i>=inx:
                                index_sym = i
                                break
                if sym == ')':
                    brackets -= 1
                    if brackets == 1:
                        for i in range(len(token_list)):
                            if token_list[i] == ',' and i>=inx:
                                index_sym = i
                                break

            return Node(token_list[0], elem._addToTree(token_list[2:index_sym]),
                        elem._addToTree(token_list[index_sym+1:-1]))

    def preOrder(elem):
        elem._preOrder(elem.root)

    def _preOrder(elem, node):
        if (node != None):
            print( str(node.data), end= ' ' )
            self._preOrder(node.left)
            self._preOrder(node.right)

    def height(elem):
        return elem._height(elem.root)
    def _height(elem, node):
        if node is None:
            return 0
        return 1 + max(elem._height(node.left), elem._height(node.right))

    def create(elem):
        print("================== BinarySearchTree ===================")
        elem._create(elem.root)
    def _create(elem, node, prefix = '', isroot = True, islast = True):
        print(prefix, end='')
        if isroot:
            print('', end='')
        else:
            if islast:
                print('└─', end='')
            else:
                print('├─', end='')
        if node:
            print(node)
        else:
            print('')


        if(not node or (not node.left and not node.right)): return
        line = [node.left, node.right]
        for i in range(len(line)):
            elem._create(line[i], prefix + ("" if isroot else ("  " if islast else '│ ')), False, i+1>=len(line))

    def lookup(elem, target):
        elem._lookup(elem.root, target)
    def _lookup(elem, node, target):
        if node == None:
            print('К сожалению, данного узла не существует')
            return False
        else:
            if target == node.data:
                print("Информация об узле:")
                print("-Значение:", node)
                print("-Левый потомок:", node.left)
                print("-Правый потомок:", node.right)
                return True
            else:
                if target < node.data: return elem._lookup(node.left, target)
                else: return elem._lookup(node.right, target)
    def insert(elem,data):
        if elem.root:
            elem._insert(data, elem.root)
        else:
            elem.root = Node(data)


    def _insert(elem, data, node):
        if data < node.data:
            if node.left:
                elem._insert(data, node.left)
            else:
                node.left = Node(data)
        else:
            if node.right:
                elem._insert(data, node.right)
            else:
                node.right = Node(data)
    # Вспомогательная функция # для поиска узла минимального значения
    def getMinimumData(elem, curr):
        while curr.left:
            curr = curr.left
        return curr

    # Функция удаления узла из BST
    def delete(elem, data):
        elem._delete(elem.root, data)
    def _delete(elem, node, data):
        parent = None
        curr = node

        while curr and curr.data != data:
            parent = curr

            if data < curr.data:
                curr = curr.left
            else:
                curr = curr.right

        if curr is None:
            return node

        # Случай 1: удаляемый узел не имеет потомков, т. е. это конечный узел.
        if curr.left is None and curr.right is None:

            if curr != node:
                if parent.left == curr:
                    parent.left = None
                else:
                    parent.right = None

            else:
                node = None

        # Случай 2: удаляемый узел имеет двух дочерних элементов
        elif curr.left and curr.right:

            # находит свой неупорядоченный узел-преемник
            successor = elem.getMinimumData(curr.right)

            # сохраняет значение преемника
            val = successor.data

            elem._delete(node, successor.data)

            # копирует значение преемника в текущий узел
            curr.data = val

        # Случай 3: удаляемый узел имеет только одного потомка
        else:

            # выбирает дочерний узел
            if curr.left:
                child = curr.left
            else:
                child = curr.right


            if curr != node:
                if curr == parent.left:
                    parent.left = child
                else:
                    parent.right = child


            else:
                node = child

        return node

    def main(elem):
        elem.create()
        print("Список комманд:")
        print(">>S: Поиск нода")
        print(">>I: Добавление нового нода")
        print(">D: Удаление нода")
        print(">E: Выход из программы")
        value = input("Введите букву: ")
        if value == 'S':
            print("\n==========Search===========")
            x = int(input('Введите значение нода: '))
            elem.lookup(x)
            elem.main()
        elif value == 'I':
            print("\n==========Insert===========")
            x = int(input('Введите значение нода: '))
            elem.insert(x)
            elem.main()
        elif value == 'D':
            print("\n==========Delete===========")
            x = int(input('Введите значение нода: '))
            elem.delete(x)
            elem.main()
        elif value == 'E':
            elem.create()
            return
        else:
            print("Такой команды не существует!")
            elem.main()

#main
binTree = BinarySearchTree('8(3(1,6(4,7)),10(,14(13,)))')
binTree.main()
