class Node:
    def init(elem, data=None, left=None, right=None):
        elem.data = data
        elem.left = left
        elem.right = right


    def str(elem):
        return str(elem.data)

class Tree:
    def init(elem, expression):
        elem.root = None
        elem.add(expression)

    def add(elem, expression):
        nums = '0123456789'
        num = ''
        token_list = []
        for n in expression:
            if n in nums:
                num += n
            elif num:
                token_list.append(int(num))
                num = ''
                token_list.append(n)
            else:
                token_list.append(n)

        elem.root = elem._addToTree(token_list)

    def _addToTree(elem, token_list):
        if token_list:
            brackets = 0
            index_sym = 0
            for inx, sym in enumerate(token_list):
                if sym == '(':
                    brackets += 1
                    if brackets == 1:
                        for i in range(len(token_list)):
                            if token_list[i] == ',' and i>=inx:
                                index_sym = i
                                break
                if sym == ')':
                    brackets -= 1
                    if brackets == 1:
                        for i in range(len(token_list)):
                            if token_list[i] == ',' and i>=inx:
                                index_sym = i
                                break

            return Node(token_list[0], elem.addToTree(token_list[2:index_sym]),
                        elem.addToTree(token_list[index_sym+1:-1]))
   
    def iterativePreOrder(elem):
        elem._iterativePreOrder(elem.root)

    def _iterativePreOrder(elem, node):
        if (node != None):
            stack = []
            stack.append(node)
            while(stack):
                newNode = stack.pop()
                print( str(newNode), end = ' ' )
                if (newNode.right): stack.append(newNode.right)
                if (newNode.left): stack.append(newNode.left)



#main

tree = Tree('8(3(1,6(4,7)),10(,14(13,)))')


print('Нерекурсивный прямой обход')
tree.iterativePreOrder()
