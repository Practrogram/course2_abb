class HashTable:
    def init(map, s):
        map.size = s
        map.slots = [None] * map.size
        map.data = [0] * map.size
        for i in range(map.size):
            map.data[i] = [None]
    def put(map, key, data):
        hash = map.hashfunction(key, len(map.slots))
        map.slots[hash] = key
        if map.data[hash] == [None]:
            map.data[hash].clear()
        map.data[hash].append(data)

    def hashfunction(map, key, size):
        return key % size


    def get(map ,key):
        startslot = map.hashfunction(key, len(map.slots))

        data = None
        stop = False
        found = False
        position = startslot
        while map.slots[position] != None and  \
        not found and not stop:
            if map.slots[position] == key:
                found = True
                data = map.data[position]
            else:
                position=map.rehash(position,len(map.slots))
                if position == startslot:
                    stop = True
        return data


#main

ma = []

with open('input.txt','r') as file:
    for line in file:
        for word in line.split():
            ma.append(word)

Ht = HashTable(len(ma))

for key in range(len(ma)):
    Ht.put(key, ma[key])


f = open('hashtable.txt', 'w')
f.write("========== HashTable ==========\n")
for i in range (len(Ht.slots)):
    f.write(str(Ht.slots[i]))
    f.write(": ")
    f.write(str(Ht.data[i]))
    f.write("\n")
f.close()